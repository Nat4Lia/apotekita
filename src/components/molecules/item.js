import React, {Component} from 'react';
import {TouchableOpacity, Image} from 'react-native';
import {ItemName, ItemDescription, ItemPrice} from '../atoms';
import { Colors, ItemStyle } from '../../styles';

class Item extends Component {
    // constructor(props) {
    //     super(props);
    // }

    currencyFormat(num) {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };

    render() {
        let produk = this.props.produk;
        const { navigation } = this.props;
        return (
            <TouchableOpacity 
                style={ItemStyle.container} 
                activeOpacity={0.8}
                onPress={() => navigation.navigate('Detail', {
                        screen: 'Detail',
                        params: {
                            itemId: produk.id,
                            itemName: produk.name,
                            itemDesc: produk.deskripsi,
                            itemPrice: produk.harga,
                            itemImg: produk.thumbnails,
                            itemQty: '0'
                        }
                    })
                }
            >
                <Image source={{uri: produk.thumbnails}} style={ItemStyle.img}/>
                <ItemName label={produk.name}/>
                <ItemDescription label={produk.deskripsi}/>
                <ItemPrice label={this.currencyFormat(Number(produk.harga))}/>
            </TouchableOpacity>
        )
    }
}

export default Item;