import * as Typography from './typography';

export const btnCont = {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 8
}

export const btnText = {
    fontFamily: Typography.MEDIUM,
    fontSize: 14,
    color: '#fff'
}