export const SEMIBOLD = 'Montserrat-SemiBold';
export const MEDIUM = 'Montserrat-Medium';
export const BOLD = 'Montserrat-Bold';
export const REGULAR = 'Montserrat-Regular';
export const ITALIC = 'Montserrat-Italic';