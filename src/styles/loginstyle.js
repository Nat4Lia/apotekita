import * as Colors from './colors';
import * as Typography from './typography';

export const container = {
    justifyContent: 'center',
    paddingTop: 48,
    paddingHorizontal: 63,
}

export const logo = {
    width: 288,
    height: 288
}

export const titleCont = {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -25,
    marginBottom: 40,
}

export const titleFirst = {
    fontFamily: Typography.MEDIUM,
    fontSize: 36,
    color: Colors.GRAY_DARK
}

export const titleSecond = {
    fontFamily: Typography.BOLD,
    fontSize: 36,
    color: Colors.PRIMARY
}

export const emailInput = {
    marginBottom: 20
}

export const passwordInput = {
    marginBottom: 20
}