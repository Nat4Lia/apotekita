import React, { Component } from 'react';
import { TouchableOpacity, Text } from 'react-native';
import { Colors, ButtonStyle } from '../../styles';

class Button extends Component {
    render() {
        return (
            <TouchableOpacity 
                {...this.props}
                style={
                    [
                        ButtonStyle.btnCont, 
                        this.props.style
                    ]
                }
                activeOpacity={0.8}
            >
                <Text style={ButtonStyle.btnText}>{this.props.label}</Text>
            </TouchableOpacity>
        )
    }
}

export default Button;
