import * as Colors from './colors';

export const container = {
    borderWidth: .2,
    borderColor: Colors.GRAY_DARK,
    borderRadius: 8,
    padding: 12,
    width: 171,
    minHeight: 250,
    backgroundColor: '#fff',
    alignItems: 'flex-start'
}

export const img = {
    borderRadius: 25,
    width: 147,
    height: 147,
    marginBottom: 6
}