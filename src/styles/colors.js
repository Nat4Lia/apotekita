export const GRAY_DARK = '#A0A2B3';
export const PRIMARY = '#129A7F';
export const BACKGROUND = '#F4F4F6';
export const LIGHT_GREEN = '#59B8A5';
export const WHITE_25A = 'rgba(255,255,255,.25)';
export const GRAY_DARKER = '#777A95';
export const LIGHT_ORANGE = '#FFA973';