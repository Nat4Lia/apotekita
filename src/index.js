import React from 'react';
import Index from './navigations';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import {store} from './redux/stores';

const App = () => (
    <Provider store={store}>
        <Index/>
    </Provider>
)
    
export default App;