import React, { Component } from 'react';
import { View, TextInput, Text } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Colors, InputTextStyle } from '../../styles';

class LabelTextInput extends Component {
  render(){
    return (
      <View style={this.props.style}>
        <View style={InputTextStyle.labelContainer}>
          <Icon name={this.props.iconName} size={16} color={Colors.GRAY_DARK} style={InputTextStyle.iconLabel}/>
          <Text style={InputTextStyle.label}>{this.props.label}</Text>
        </View>
        <View style={InputTextStyle.textInputCont}>
          <TextInput
            {...this.props}
            style={InputTextStyle.textInput}
          />
          <View style={InputTextStyle.line}></View>
        </View>
      </View>
    )
  }
}

export default LabelTextInput;