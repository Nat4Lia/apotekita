import * as Colors from './colors';
import * as Typography from './typography';

export const container = {
    padding: 24,
    paddingTop: 12,
    justifyContent: 'center',
    backgroundColor: Colors.BACKGROUND,
}

export const itemCont = {
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 6,
    flexDirection: 'row',
}

export const img = {
    width: 67,
    height: 67,
    borderRadius: 4
}

export const textCont = {
    marginLeft: 12,
    justifyContent: 'center'
}

export const text = {
    color: Colors.GRAY_DARKER
}

export const itemName = {
    fontFamily: Typography.MEDIUM,
    fontSize: 24,
    marginTop: -12,
    marginBottom: 2
}

export const itemPrice = {
    fontFamily: Typography.MEDIUM,
    fontSize: 18,
}

export const counterCont = {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    flex: 1
}

export const counterText = {
    fontFamily: Typography.REGULAR,
    fontSize: 18,
    marginHorizontal: 8
}