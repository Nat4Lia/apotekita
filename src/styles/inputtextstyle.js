import * as Colors from './colors';
import * as Typography from './typography';


export const labelContainer = {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingLeft: 4,
}

export const iconLabel = {
    marginTop: 2
}

export const label = {
    marginLeft: 8,
    color: Colors.GRAY_DARK,
    fontFamily: Typography.SEMIBOLD,
    fontSize: 14
}

export const textInputCont = {
    marginTop: -10
}

export const textInput = {
    color: Colors.GRAY_DARK,
    fontFamily: Typography.MEDIUM,
    fontSize: 16
}

export const line = {
    marginTop: -5,
    marginHorizontal: 4,
    borderBottomWidth : 1,
    borderColor : Colors.GRAY_DARK
}