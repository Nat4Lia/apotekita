import React, { Component } from 'react';
import {View, Text, Image, KeyboardAvoidingView, ActivityIndicator} from 'react-native';

import {LabelTextInput, Button} from '../../components/atoms';
import {Colors, LoginStyle} from '../../styles';

import Axios from 'axios';

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false,
      userName: '',
      password: ''
    };
  }

  componentDidMount() {
    this.getFirebaseData()
  }

  getFirebaseData = async () => {
    try {
      const response = await Axios.get(`https://apotekita-2020.firebaseio.com/.json`)
      this.setState({ isError: false, isLoading: false, data: response.data })
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }

  loginHandler() {
    // console.log('Pressed');
    this.props.navigation.navigate('Home', {userName: this.state.userName, data: this.state.data})
    // if (this.state.password === this.state.data.user.password && this.state.userName === this.state.data.user.email) {
    //   this.props.navigation.navigate('Home', {userName: this.state.userName, data: this.state.data})
    // }
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    } else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
    return (
      <KeyboardAvoidingView style={LoginStyle.container} behavior="position">
          <Image 
            source={require("../../assets/img/logo-apotek.png")}
            style={LoginStyle.logo}  
          />
          <View style={LoginStyle.titleCont}>
            <Text style={LoginStyle.titleFirst}>Apote</Text>
            <Text style={LoginStyle.titleSecond}>K</Text>
            <Text style={LoginStyle.titleFirst}>ita</Text>
          </View>
          <LabelTextInput 
            iconName="mail" 
            label="Email" 
            style={LoginStyle.emailInput}
            onChangeText={userName => this.setState({ userName })}
          />
          <LabelTextInput 
            iconName="lock-open" 
            label="Password" 
            style={LoginStyle.passwordInput}
            secureTextEntry={true}
            onChangeText={password => this.setState({ password })}
          />
          <Button 
            label="Login" 
            style={{backgroundColor: Colors.PRIMARY}}
            onPress={
              ({route, navigation}) => this.loginHandler({route, navigation})
            }
          />
      </KeyboardAvoidingView>
    );
  }
};

export default LoginScreen;