import { createStore } from 'redux'
import { reducer } from '../reducers';

// Mendefinisikan store menggunakan reducer
export const store = createStore(reducer);