import * as Colors from './colors';
import * as Typography from './typography';

export const container = {
    padding: 12,
    paddingTop: 0,
    justifyContent: 'center',
    backgroundColor: Colors.BACKGROUND,
}

export const header = {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: 12,
    marginBottom: 0
}

export const textWrapper = {
    flexDirection: 'row',
    alignItems: 'center',
}

export const greeting = {
    fontFamily: Typography.REGULAR,
    fontSize: 36,
    color: Colors.GRAY_DARK
}

export const userName = {
    fontFamily: Typography.SEMIBOLD,
    fontSize: 36,
    color: Colors.GRAY_DARK
}

export const avatar = {
    width: 44,
    height: 44,
    borderRadius: 44,
}