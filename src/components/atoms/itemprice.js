import React, {Component} from 'react';
import {Text} from 'react-native';
import { Colors, Typography } from '../../styles';
class ItemPrice extends Component {
    render() {
        return (
            <Text 
                style={{
                    fontFamily: Typography.SEMIBOLD,
                    fontSize: 18,
                    color: Colors.GRAY_DARK,
                }}
            >{this.props.label}</Text>
        )
    }
}
export default ItemPrice;