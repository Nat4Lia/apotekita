import * as Colors from './colors';
import * as Typography from './typography';

export const container = {
    padding: 12,
    justifyContent: 'center',
    backgroundColor: Colors.BACKGROUND,
}

export const header = {
    margin: 12,
    flexDirection: 'row'
}

export const avatar = {
    width: 72,
    height: 72,
    borderRadius: 8,
    marginRight: 12
}

export const userName = {
    fontFamily: Typography.SEMIBOLD,
    fontSize: 24,
    color: Colors.GRAY_DARK,
    marginBottom: 3,
}

export const userVerified = {
    fontFamily: Typography.ITALIC,
    fontSize: 12,
    color: Colors.PRIMARY
}

export const userEmail = {
    fontFamily: Typography.REGULAR,
    fontSize: 14,
    color: Colors.GRAY_DARK
}

export const counterCont = {
    backgroundColor: Colors.LIGHT_GREEN,
    borderTopLeftRadius: 26,
    borderTopRightRadius: 26,
    height: 158,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 24,
    marginHorizontal: -12,
    marginTop: 12,
}

export const countIsiCont = {
    flexDirection: 'row',
    marginTop: -24,
}

export const countTextCont = {
    marginLeft: 6,
    justifyContent: 'center',
}

export const countTitle = {
    fontFamily: Typography.MEDIUM,
    fontSize: 14,
    color: '#fff'
}

export const countIsi = {
    fontFamily: Typography.BOLD,
    fontSize: 16,
    color: '#fff'
}

export const iconRounded = {
    backgroundColor: Colors.WHITE_25A,
    width: 54,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 27
}

export const dataCont = {
    borderTopLeftRadius: 26,
    borderTopRightRadius: 26,
    marginTop: -24,
    marginHorizontal: -12,
    backgroundColor: Colors.BACKGROUND,
    padding: 24,
}

export const dataTextCont = {
    marginBottom: 6
}

export const dataTitle = {
    fontFamily: Typography.REGULAR,
    fontSize: 12,
    color: Colors.GRAY_DARK
}

export const dataIsi = {
    fontFamily: Typography.SEMIBOLD,
    fontSize: 20,
    color: Colors.GRAY_DARK
}