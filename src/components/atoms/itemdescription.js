import React, {Component} from 'react';
import {Text} from 'react-native';
import { Colors, Typography } from '../../styles';
class ItemDescription extends Component {
    render() {
        return (
            <Text 
                style={{
                    fontFamily: Typography.REGULAR,
                    fontSize: 10,
                    color: Colors.GRAY_DARK,
                    minHeight: 26
                }}
            >{this.props.label}</Text>
        )
    }
}
export default ItemDescription;