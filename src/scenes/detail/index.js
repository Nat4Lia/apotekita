import React, { Component } from 'react';
import {View, Text, Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import data from '../../data.json';
import {DetailStyle} from '../../styles';

class DetailScreen extends Component {
    constructor(props) {
        super(props);
    }

    currencyFormat(num) {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };

    render() {
        const ITEM = {
            ID: this.props.route.params.itemId,
            NAME: this.props.route.params.itemName,
            DESC: this.props.route.params.itemDesc,
            PRICE: this.props.route.params.itemPrice,
            IMG: this.props.route.params.itemImg
        }
        return(
            <View style={DetailStyle.container}>
                <Image source={{uri: ITEM.IMG}} style={DetailStyle.imgItem}/>
                <View style={DetailStyle.textCont}>
                    <Text style={[DetailStyle.text, {fontSize: 24, marginBottom: 6}]}>{ITEM.NAME}</Text>
                    <Text style={[DetailStyle.text, {fontSize: 12, marginBottom: 6}]}>{ITEM.DESC}</Text>
                    <Text style={[DetailStyle.text, {fontSize: 18}]}>{this.currencyFormat(Number(ITEM.PRICE))}</Text>
                </View>
            </View>
        )
    }
} 

export default DetailScreen;