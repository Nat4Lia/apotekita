// import {createStackNavigator} from 'react-navigation-stack';

// import LoginScreen from '../scenes/login';

// const AuthNavigatorConfig = {
//   initialRouteName: 'Login',
//   header: null,
//   headerMode: 'none',
// };

// const RouteConfigs = {
//   Login:LoginScreen,
// };

// const AuthNavigator = createStackNavigator(RouteConfigs, AuthNavigatorConfig);

// export default AuthNavigator;

import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { LoginScreen } from '../scenes';

const LoginStack = createStackNavigator();

const LoginStackScreen = () => (
    <LoginStack.Navigator>
        <LoginStack.Screen name="Login" component={LoginScreen} options={{headerShown: false}}/>
        <LoginStack.Screen name="Home" component={LoginScreen} options={{headerShown: false}}/>
    </LoginStack.Navigator>
)

export default LoginStackScreen;