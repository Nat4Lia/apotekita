import * as Typography from './typography';
import * as Colors from './colors';

export const btnCont = {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 8,
    padding: 12
}

export const btnText = {
    fontFamily: Typography.SEMIBOLD,
    fontSize: 18,
}

export const btnActive = {
    backgroundColor: Colors.PRIMARY
}

export const textActive = {
    color: '#fff'
}

export const btnInactive = {
    borderWidth: 1,
    borderColor: Colors.GRAY_DARK,
    backgroundColor: '#fff'
}

export const textInactive = {
    color: Colors.GRAY_DARK
}