import * as Colors from './colors';
import * as Typography from './typography';

export const container = {
    padding: 24,
    paddingTop: 12,
    justifyContent: 'center',
    backgroundColor: Colors.BACKGROUND,
}

export const imgItem = {
    width: 363,
    height: 363,
    borderRadius: 8,
    marginBottom: 12
}

export const textCont = {
    backgroundColor: Colors.LIGHT_GREEN,
    padding: 12,
    borderRadius: 8
}

export const text = {
    fontFamily: Typography.MEDIUM,
    color: '#fff'
}
