import React, {Component} from 'react';
import {View, FlatList, Text, Image} from 'react-native';
import {Item} from '../../components/molecules';
import {ButtonCategory} from '../../components/atoms';
import {Colors, HomeStyle} from '../../styles';
// import data from '../../data.json';

class HomeScreen extends Component {
    render() {
        const data = this.props.route.params.params.data;
        return(
            <View style={HomeStyle.container}>
                <View style={HomeStyle.header}>
                    <View style={HomeStyle.textWrapper}>
                        <Text style={HomeStyle.greeting}>Hi, </Text>
                        <Text style={HomeStyle.userName}>{data.user.firstName}</Text>
                    </View>
                    <Image source={{uri: data.user.avatar}} style={HomeStyle.avatar}/>
                </View>
                <FlatList
                    data={data.categories}
                    renderItem={(kategori) => (
                            <View style={{margin: 12}}>
                                <ButtonCategory kategori={kategori.item}/>
                            </View>
                        )
                    }
                    keyExtractor={(item) => item.id}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                />
                <FlatList
                    data={data.items}
                    renderItem={
                        (produk) => <Item produk={produk.item} navigation={this.props.navigation} />
                    }
                    inverted={false}
                    ItemSeparatorComponent={() => (<View style={{margin: 10}}></View>)}
                    keyExtractor={(item) => item.id}
                    numColumns={2}
                    style={{margin: 12, paddingTop: -12, marginBottom: 58}}
                    columnWrapperStyle={{justifyContent: 'space-between'}}
                />
            </View>
        )
    }
} 
export default HomeScreen;