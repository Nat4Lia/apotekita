import React, { Component } from 'react';
import {View, Text, Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
// import data from '../../data.json';
import {Colors, ProfileStyle} from '../../styles';

const ProfileScreen = ({navigation, route}) => {
    const data = route.params.params.data
    return (
    <View style={ProfileStyle.container}>
        <View style={ProfileStyle.header}>
            <Image source={{uri: data.user.avatar}} style={ProfileStyle.avatar}/>
            <View style={ProfileStyle.title}>
                <Text style={ProfileStyle.userName}>{data.user.firstName + " " + data.user.lastName}</Text>
                <Text style={ProfileStyle.userVerified}>Verified Account</Text>
                <Text style={ProfileStyle.userEmail}>{data.user.email}</Text>
            </View>
        </View>
        <View style={ProfileStyle.counterCont}>
            <View style={ProfileStyle.countIsiCont}>
                <View style={ProfileStyle.iconRounded}>
                    <Icon name="wallet-outline" size={33} color={'#fff'} />
                </View>
                <View style={ProfileStyle.countTextCont}>
                    <Text style={ProfileStyle.countTitle}>Saldo</Text>
                    <Text style={ProfileStyle.countIsi}>Rp 500.000</Text>
                </View>
            </View>

            <View style={ProfileStyle.countIsiCont}>
                <View style={ProfileStyle.iconRounded}>
                    <Icon name="basket-outline" size={33} color={'#fff'} />
                </View>
                <View style={ProfileStyle.countTextCont}>
                    <Text style={ProfileStyle.countTitle}>Total Belanja</Text>
                    <Text style={ProfileStyle.countIsi}>759 Kali</Text>
                </View>
            </View>
        </View>
        <View style={ProfileStyle.dataCont}>
            <View style={ProfileStyle.dataTextCont}>
                <Text style={ProfileStyle.dataTitle}>Tanggal Lahir</Text>
                <Text style={ProfileStyle.dataIsi}>{data.user.bornDate}</Text>
            </View>

            <View style={ProfileStyle.dataTextCont}>
                <Text style={ProfileStyle.dataTitle}>Jenis Kelamin</Text>
                <Text style={ProfileStyle.dataIsi}>{data.user.gender}</Text>
            </View>

            <View style={ProfileStyle.dataTextCont}>
                <Text style={ProfileStyle.dataTitle}>Alamat</Text>
                <Text style={ProfileStyle.dataIsi}>{data.user.address}</Text>
                <Text style={ProfileStyle.dataIsi}>
                    {data.user.district + ", " + data.user.city + ", " + data.user.poscode}
                </Text>
                <Text style={ProfileStyle.dataIsi}>{data.user.province}</Text>
            </View>

            <View style={ProfileStyle.dataTextCont}>
                <Text style={ProfileStyle.dataTitle}>Nomor Hp</Text>
                <Text style={ProfileStyle.dataIsi}>{data.user.phoneNumber}</Text>
            </View>
        </View>
    </View>
)}

export default ProfileScreen;