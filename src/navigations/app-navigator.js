// import {createBottomTabNavigator} from 'react-navigation-tabs';

// import HomeScreen from '../scenes/home';
// import AboutScreen from '../scenes/about';

// const TabNavigatorConfig = {
//   initialRouteName: 'Home',
//   header: null,
//   headerMode: 'none',
// };

// const RouteConfigs = {
//   Home:{
//     screen:HomeScreen,
//   },
//   About:{
//     screen:AboutScreen,
//   },
// };

// const AppNavigator = createBottomTabNavigator(RouteConfigs, TabNavigatorConfig);

// export default AppNavigator;

import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { HomeScreen } from '../scenes';

const HomeStack = createStackNavigator();

const HomeStackScreen = ({ navigator }) => (
    <HomeStack.Navigator>
        <HomeStack.Screen name="Home" component={HomeScreen}/>
    </HomeStack.Navigator>
)

export default HomeStackScreen;