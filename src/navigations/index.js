// import {createAppContainer, createSwitchNavigator} from 'react-navigation';

// import AuthNavigator from './auth-navigator';
// import AppNavigator from './app-navigator';

// const RootNavigator = createSwitchNavigator(
//   {
//     Auth: AuthNavigator,
//     App: AppNavigator,
//   },
//   {
//     initialRouteName: 'Auth',
//   },
// );

// export default createAppContainer(RootNavigator);

import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import {Button} from '../components/atoms';
import { LoginScreen, HomeScreen, ProfileScreen, DetailScreen, CartScreen } from '../scenes';
import {Colors, Typography} from '../styles';
import { connect } from 'react-redux';
import { addItem } from '../redux/actions';

const RootStack = createStackNavigator();

const TabsStack = createBottomTabNavigator();

const AppStackScreen = ({navigation, route}) => (
    <TabsStack.Navigator
        screenOptions={
            ({route}) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
                    if (route.name === 'Home') {
                        iconName = 'home';
                    } else if (route.name === 'Profile') {
                        iconName = 'person-circle';
                    } else if (route.name === 'Cart') {
                        iconName = 'cart';
                    }
                    
                    return (
                        <View style={{
                            backgroundColor: focused ? Colors.LIGHT_GREEN : '#fff',
                            paddingLeft: 9,
                            paddingRight: 9,
                            paddingVertical: 2,
                            borderRadius: 8,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <Icon name={iconName} size={size} color={color} style={{marginLeft: 2}} />
                            <Text style={{
                                color: color,
                                fontFamily: Typography.LIGHT,
                                fontSize: 10
                            }}>{route.name}</Text>
                        </View>
                    )
                }
            })
        }
        tabBarOptions={{
            activeTintColor: '#fff',
            inactiveTintColor: Colors.GRAY_DARK,
            showLabel: false
        }}
    >
        <TabsStack.Screen name="Home" component={HomeScreen} initialParams={route}/>
        <TabsStack.Screen name='Cart' component={CartStackScreen} options={{tabBarVisible: false}}/>
        <TabsStack.Screen name="Profile" component={ProfileScreen} initialParams={route}/>
    </TabsStack.Navigator>
)

const CartTabStackScreen = () => (
    <TabsStack.Navigator>
        <TabsStack.Screen name="Bayar" component={CartScreen}/>
    </TabsStack.Navigator>
)

const CartStackScreen = ({ navigation }) => (
    <RootStack.Navigator initialRouteName="Cart">
        <RootStack.Screen name='Cart' component={CartTabStackScreen} options={{
            headerStyle: {
                backgroundColor: 'transparent',
                elevation: 0
            },
            headerLeft: () => (
                <TouchableOpacity 
                    style={{
                        marginLeft: 24, 
                        borderRadius: 4, 
                        borderColor: Colors.GRAY_DARK,
                        borderWidth: 1,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }} 
                    activeOpacity={0.8}
                    onPress={() => navigation.goBack()}
                >
                    <Icon name="arrow-back" size={28} color={Colors.GRAY_DARKER}/>
                </TouchableOpacity>
            ),
            headerTitleStyle: {
                fontFamily: Typography.MEDIUM,
                fontSize: 24,
                color: Colors.GRAY_DARKER
            },
            headerTitleAlign: 'center',
            headerTitle: 'Daftar Belanja'
        }}/>
    </RootStack.Navigator>
)

const DetailStackScreen = ({navigation, route}) => (
    <TabsStack.Navigator initialRouteName='Detail' tabBarOptions={{
        style: {
            paddingHorizontal: 20,
        }
    }}>
        <TabsStack.Screen name='Cart' component={CartStackScreen} options={{
                tabBarButton: () => (
                <Button 
                    label="Beli" 
                    style={{
                        backgroundColor: Colors.LIGHT_ORANGE,
                        flex: 1,
                        margin: 4
                    }}
                    onPress={() => (
                        route.params.props.addItem(route.params.params),
                        navigation.setOptions({ title: 'Daftar Belanja' }),
                        navigation.navigate('Cart')
                    )}
                />),
                tabBarVisible: false
            }}
        />
        
        <TabsStack.Screen name='Detail' component={DetailScreen} options={{
                tabBarButton: props => <Button 
                    label="Tambah" 
                    style={{
                        backgroundColor: Colors.LIGHT_ORANGE,
                        flex: 1,
                        marginVertical: 4
                    }}
                />
            }}
        />
    </TabsStack.Navigator>
)

class Index extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <NavigationContainer>
                <RootStack.Navigator >
                    <RootStack.Screen name='Login' component={LoginScreen} options={{headerShown: false}}/>
                    <RootStack.Screen name='Home' component={AppStackScreen} options={{headerShown: false}}/>
                    {/* <RootStack.Screen name='Cart' component={CartStackScreen} /> */}
                    <RootStack.Screen name='Detail' component={DetailStackScreen} 
                        options={{
                            headerShown: true,
                            headerStyle: {
                                backgroundColor: 'transparent',
                                elevation: 0
                            },
                            headerLeft: (props) => (
                                <TouchableOpacity 
                                    style={{
                                        marginLeft: 24, 
                                        borderRadius: 4, 
                                        borderColor: Colors.GRAY_DARK,
                                        borderWidth: 1,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }} 
                                    activeOpacity={0.8}
                                    {...props}
                                >
                                    <Icon name="arrow-back" size={28} color={Colors.GRAY_DARKER}/>
                                </TouchableOpacity>
                            ),
                            headerTitleStyle: {
                                fontFamily: Typography.MEDIUM,
                                fontSize: 24,
                                color: Colors.GRAY_DARKER
                            },
                            headerTitleAlign: 'center',
                            title: false
                        }}
                        initialParams={{props: this.props}}
                    />
                </RootStack.Navigator>
            </NavigationContainer>
        )
    }
} 

function mapStateToProps(state) {
    return {
        count: state
    }
}

export default connect(mapStateToProps, { addItem })(Index);