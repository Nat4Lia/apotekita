import React, {Component} from 'react';
import {TouchableOpacity, Text} from 'react-native';
import { Colors, ButtonCategoryStyle } from '../../styles';

class ButtonCategory extends Component {
    render() {
        return (
            <TouchableOpacity 
                style={
                    [
                        ButtonCategoryStyle.btnCont,
                        this.props.isActive ? ButtonCategoryStyle.btnActive : ButtonCategoryStyle.btnInactive
                    ]
                }
                activeOpacity={0.8}
            >
                <Text 
                    style={
                        [
                            ButtonCategoryStyle.btnText,
                            this.props.isActive ? ButtonCategoryStyle.textActive : ButtonCategoryStyle.textInactive
                        ]
                    }
                >{this.props.kategori.name}</Text>
            </TouchableOpacity>
        )
    }
}

export default ButtonCategory;


