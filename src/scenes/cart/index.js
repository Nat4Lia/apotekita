import React, { Component } from 'react';
import {View, Text, Image, TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import data from '../../data.json';
import {CartStyle} from '../../styles';
import { Colors } from '../../styles';
import { connect } from 'react-redux';

class Item extends Component {

    currencyFormat(num) {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };
    
    render() {
        let produk = this.props.produk;

        return (
            <View style={CartStyle.itemCont}>
                <Image source={{uri: produk.itemImg}} style={CartStyle.img} />
                <View style={CartStyle.textCont}>
                    <Text style={[CartStyle.text, CartStyle.itemName]}>{produk.itemName}</Text>
                    <Text style={[CartStyle.text, CartStyle.itemPrice]}>{this.currencyFormat(Number(produk.itemPrice))}</Text>
                </View>
                <View style={CartStyle.counterCont}>
                    <TouchableOpacity>
                        <Icon name='remove-circle' size={28} color={Colors.GRAY_DARKER} />
                    </TouchableOpacity>
                    <Text style={[CartStyle.text, CartStyle.counterText]}>999</Text>
                    <TouchableOpacity>
                        <Icon name='add-circle' size={28} color={Colors.GRAY_DARKER} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
} 

class CartScreen extends Component {
    constructor(props) {
        super(props);
    }
    
    render() {
        console.log(this.props.data)
        return(
            <View style={CartStyle.container}>
                <FlatList 
                    data={this.props.data.items}
                    renderItem={
                        (produk) => <Item produk={produk.item} />
                    }
                    keyExtractor={(item) => item.itemId}
                    ItemSeparatorComponent={() => (<View style={{margin: 6}}></View>)}
                />
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        data: state
    }
}

export default connect(mapStateToProps)(CartScreen);